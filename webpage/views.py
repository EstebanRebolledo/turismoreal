from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def index(request):
    context= {}
    mensaje = ""
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('logeado')
        else:
            mensaje = "error de login"
    return render(request,'index.html', context)

def logeado(request):
    return render(request,'logeado.html')

def logoutUser(request):
    logout(request)
    return redirect('index')