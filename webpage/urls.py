from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name="index"),
    path('logeado/',views.logeado, name="logeado"),
    path('logout/',views.logoutUser, name="logout"),
]